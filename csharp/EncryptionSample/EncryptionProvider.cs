//
//  Copyright 2013  Vadi
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace EncryptionSample
{
	public class EncryptionProvider : IDisposable {

		private DESCryptoServiceProvider cryptoProvider = null;
		private byte[] byteKey = null;
		private byte[] byteSpec = null;

		public  EncryptionProvider (String key, String spec) {
			cryptoProvider = new DESCryptoServiceProvider ();
			byteKey = Convert.FromBase64String (key);
			byteSpec = Convert.FromBase64String (spec);
		}

		public  String Encrypt(string originalString)
		{
			String result = null;

			if (String.IsNullOrEmpty(originalString))
			{
				throw new ArgumentNullException ("The string which needs to be encrypted can not be null.");
			}

			using (MemoryStream memoryStream = new MemoryStream()) {
				CryptoStream cryptoStream = new CryptoStream (memoryStream, 
				                                            cryptoProvider.CreateEncryptor (byteKey, byteSpec), 
				                                            CryptoStreamMode.Write);

				StreamWriter writer = new StreamWriter (cryptoStream);
				writer.Write (originalString);
				writer.Flush ();
				cryptoStream.FlushFinalBlock ();
				writer.Flush ();

				result = Convert.ToBase64String (memoryStream.GetBuffer (), 0, (int)memoryStream.Length);
			}

			return result;
		}

		public  String Decrypt (String value) {

			String result = null;

			if (String.IsNullOrEmpty(value))
			{
				throw new ArgumentNullException ("The string which needs to be decrypted can not be null.");
			}

			using (MemoryStream memoryStream = new MemoryStream (Convert.FromBase64String(value))) {

				CryptoStream cryptoStream = new CryptoStream (
					memoryStream, 
				    cryptoProvider.CreateDecryptor (byteKey, byteSpec), 
				    CryptoStreamMode.Read);

				StreamReader reader = new StreamReader (cryptoStream);
				result = reader.ReadToEnd ();
			}

			return result;
		}

		#region IDisposable implementation

		public void Dispose ()
		{
			if (cryptoProvider != null) {
				cryptoProvider.Dispose ();
			}
		}

		#endregion
	}
}
