//
//  Copyright 2013  Vadi
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Text;

namespace EncryptionSample
{
	class MainClass
	{
		// NOTE: This values are base64'ed since the keys are generated
		// using strong key providers of the same Cryptography classes
		static String theSharedSecretKey = "5HbIyrcqNaY=";
		static String theSharedIV = "HqG5BsDeUbE=";

		public static void Main (string[] args)
		{
			EncryptionProvider provider = new EncryptionProvider (theSharedSecretKey, theSharedIV);


			// Below value is copy pasted from Java output
			// encryptedValue    -> gb6TmhO+keeIZ/Q0PiU2Uw==
			// decryptedValue    -> Hello World!
			Console.WriteLine(provider.Decrypt ("gb6TmhO+keeIZ/Q0PiU2Uw=="));

			Console.ReadLine ();
		}
	}

}
