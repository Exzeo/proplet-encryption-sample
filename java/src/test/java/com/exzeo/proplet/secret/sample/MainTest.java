package com.exzeo.proplet.secret.sample;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple Main.
 */
public class MainTest
    extends TestCase
{
    public void testEncryptionBasics() throws EncryptionProvider.EncryptionException {

        // NOTE: This values are base64'ed since the keys are generated
        // using strong key providers of the same Cryptography classes
        final String theSharedSecretKey = "5HbIyrcqNaY=";
        final String theSharedIV = "HqG5BsDeUbE=";

        final String simpleValue = "Hello World!";

        final EncryptionProvider encryptionProvider =
                new EncryptionProvider(theSharedSecretKey, theSharedIV);

        final String encryptedValue  = encryptionProvider.encrypt(simpleValue);
        final String decryptedValue = encryptionProvider.decrypt(encryptedValue);

        assertTrue(decryptedValue.equals(simpleValue));
    }
}
