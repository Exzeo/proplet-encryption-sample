package com.exzeo.proplet.secret.sample;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Provides encryption capabilities. Uses DES encryption cipher.
 *
 */
public class EncryptionProvider {
    private Cipher deCipher;
    private Cipher enCipher;
    private SecretKeySpec key;
    private IvParameterSpec ivspec;

    public EncryptionProvider(String keyInput, String specInput) throws EncryptionException {

        byte[] keyBytes = decode (keyInput);
        byte[] ivBytes = decode (specInput);

        try {
            ivspec = new IvParameterSpec(ivBytes);
            DESKeySpec dkey = new DESKeySpec(keyBytes);
            key = new SecretKeySpec(dkey.getKey(), "DES");
            deCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            enCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            throw new EncryptionException (e);
        } catch (NoSuchPaddingException e) {
            throw new EncryptionException (e);
        } catch (InvalidKeyException e) {
            throw new EncryptionException (e);
        }
    }

    public String encrypt(String value) throws EncryptionException {

        final byte[] bytes = StringUtils.getBytesUtf8(value);

        try {
            enCipher.init(Cipher.ENCRYPT_MODE, key, ivspec);
        } catch (InvalidKeyException e) {
            throw new EncryptionException(e);
        } catch (InvalidAlgorithmParameterException e) {
            throw new EncryptionException(e);
        }

        final byte[] encrypted;

        try {
            encrypted = enCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException e) {
            throw new EncryptionException(e);
        } catch (BadPaddingException e) {
            throw new EncryptionException(e);
        }

        return encode(encrypted);
    }

    public String decrypt(String value) throws EncryptionException {

        final byte[] bytes = decode(value);

        try {
            deCipher.init(Cipher.DECRYPT_MODE, key, ivspec);
        } catch (InvalidKeyException e) {
            throw new EncryptionException(e);
        } catch (InvalidAlgorithmParameterException e) {
            throw new EncryptionException(e);
        }

        final byte[] decrypted;

        try {
            decrypted = deCipher.doFinal(bytes);
        } catch (IllegalBlockSizeException e) {
            throw new EncryptionException(e);
        } catch (BadPaddingException e) {
            throw new EncryptionException(e);
        }

        return StringUtils.newStringUtf8(decrypted);
    }


    public static byte[] decode(String s) {
        return Base64.decodeBase64(StringUtils.getBytesUtf8(s));
    }

    public static String encode(byte[] s) {
        return Base64.encodeBase64String(s);
    }

    public class EncryptionException extends Throwable {
        public EncryptionException(InvalidKeyException e) {
            super(e);
        }

        public EncryptionException(NoSuchAlgorithmException e) {
            super(e);
        }

        public EncryptionException(NoSuchPaddingException e) {
            super(e);
        }

        public EncryptionException(InvalidAlgorithmParameterException e) {
            super(e);
        }

        public EncryptionException(IllegalBlockSizeException e) {
            super(e);
        }

        public EncryptionException(BadPaddingException e) {
            super(e);
        }
    }
}
