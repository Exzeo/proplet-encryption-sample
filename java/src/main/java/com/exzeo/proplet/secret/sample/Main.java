package com.exzeo.proplet.secret.sample;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;


/**
 * Demonstrates encryption and decryption across Java and C#
 */
public class Main {
    public static void main(String[] args) throws EncryptionProvider.EncryptionException {

        // NOTE: This values are base64'ed since the keys are generated
        // using strong key providers of the same Cryptography classes
        final String theSharedSecretKey = "5HbIyrcqNaY=";
        final String theSharedIV = "HqG5BsDeUbE=";

        final String simpleValue = "Hello World!";

        final EncryptionProvider encryptionProvider =
                new EncryptionProvider(theSharedSecretKey, theSharedIV);

        final String encryptedValue  = encryptionProvider.encrypt(simpleValue);
        final String decryptedValue = encryptionProvider.decrypt(encryptedValue);

        System.out.println (String.format("encryptedValue    -> %s",encryptedValue));
        System.out.println (String.format("decryptedValue    -> %s",decryptedValue));
    }

}





